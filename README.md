# docker-php-5.6-cli with composer
Available on [Docker Hub](https://hub.docker.com/r/zerospam/php-composer)

We have some legacy projects that are running PHP 5.6 (Cumulus and provulus-sdk). This image will provide a cli to work with such projects.

Thanks to [helderco](https://github.com/helderco/docker-php-5.3) for solving the openssl headache
The rest of this repo is borrowed from the [Official PHP image](https://github.com/docker-library/php) that no longer supports 5.6
Usage should be the same as the [Official PHP cli](https://hub.docker.com/_/php/)

Usage example:

	docker run -it --rm --name composer-cli -v "$PWD":/usr/src/myapp -w /usr/src/myapp zerospam/php-composer composer install
